<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Jobs\TestJob;
use App\Jobs\ProductCreated;

class EventServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
        // \App::bindMethod(TestJob::class . '@handle', function($job) {
        //     return $job->handle();
        // });
        \App::bindMethod(ProductCreated::class . '@handle', function($job) {
            return $job->handle();
        });
        \App::bindMethod(ProductUpdated::class . '@handle', function($job) {
            return $job->handle();
        });
        \App::bindMethod(ProductDeleted::class . '@handle', function($job) {
            return $job->handle();
        });
    }
}
